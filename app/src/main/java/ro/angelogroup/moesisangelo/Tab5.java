package ro.angelogroup.moesisangelo;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;


/**
 * Created by dev on 7/12/17.
 */

public class Tab5 extends Fragment implements SearchView.OnQueryTextListener {
    private static final String TAG = "SearchViewFilterMode";

    private SearchView mSearchView;
    private ListView mListView;
    private ArrayAdapter<String> mAdapter;
    public String[] array_nume, array_price;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        array_nume = new String[MainActivity.diverse.size()];
        array_price = new String[MainActivity.diverse.size()];

        for (int i=0; i<MainActivity.diverse.size();i++){
            array_nume[i] = MainActivity.diverse.get(i).nume;
            array_price[i] = MainActivity.diverse.get(i).pret;
        }

        View view = inflater.inflate(R.layout.tab5, container, false);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSearchView = (SearchView) getView().findViewById(R.id.search_view);
        mListView = (ListView) getView().findViewById(R.id.list_view);
        mListView.setAdapter(mAdapter = new ArrayAdapter<String>(getView().getContext(),
                android.R.layout.simple_list_item_1,
                array_nume));
        mListView.setTextFilterEnabled(true);
        setupSearchView();

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long rowId) {


                // custom dialog
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Shop");

                // set the custom dialog components - text, image and button
                TextView text = dialog.findViewById(R.id.text);
                text.setText(parent.getItemAtPosition(position).toString());

                final TextView price = dialog.findViewById(R.id.total);
                final float pret = Float.parseFloat(array_price[position]);
                price.setText(pret+"");

                final EditText Field1 = dialog.findViewById(R.id.editfield);
                Field1.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void afterTextChanged(Editable s) {}

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        if(s.length() != 0) {
                            TextView qty = dialog.findViewById(R.id.qty);
                            int q = Integer.parseInt(Field1.getText().toString());
                            qty.setText(q+"");
                            price.setText(((q)*pret)+"");
                            //price.setText();
                        }
                    }
                });

                Button btnM1 = dialog.findViewById(R.id.m1);
                btnM1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TextView qty = dialog.findViewById(R.id.qty);
                        int q = Integer.parseInt(qty.getText().toString());
                        if (q>1){
                            qty.setText((q-1)+"");
                            price.setText(((q-1)*pret)+"");
                        }
                    }
                });

                Button btnP1 = dialog.findViewById(R.id.p1);
                btnP1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TextView qty = dialog.findViewById(R.id.qty);
                        int q = Integer.parseInt(qty.getText().toString());
                        if (q==1){
                            q=0;
                        }
                        qty.setText((q+1)+"");
                        price.setText(((q+1)*pret)+"");
                    }
                });

                Button btnM2 = dialog.findViewById(R.id.m5);
                btnM2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TextView qty = dialog.findViewById(R.id.qty);
                        int q = Integer.parseInt(qty.getText().toString());
                        if (q>5){
                            qty.setText((q-5)+"");
                            price.setText(((q-5)*pret)+"");
                        }
                    }
                });

                Button btnP2 = dialog.findViewById(R.id.p5);
                btnP2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TextView qty = dialog.findViewById(R.id.qty);
                        int q = Integer.parseInt(qty.getText().toString());
                        if (q==1){
                            q=0;
                        }
                        qty.setText((q+5)+"");
                        price.setText(((q+5)*pret)+"");
                    }
                });


                Button btnM3 = dialog.findViewById(R.id.m10);
                btnM3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TextView qty = dialog.findViewById(R.id.qty);
                        int q = Integer.parseInt(qty.getText().toString());
                        if (q>10){
                            qty.setText((q-10)+"");
                            price.setText(((q-10)*pret)+"");
                        }
                    }
                });

                Button btnP3 = dialog.findViewById(R.id.p10);
                btnP3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TextView qty = dialog.findViewById(R.id.qty);
                        int q = Integer.parseInt(qty.getText().toString());
                        if (q==1){
                            q=0;
                        }
                        qty.setText((q+10)+"");
                        price.setText(((q+10)*pret)+"");
                    }
                });


                final Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView text = dialog.findViewById(R.id.text);
                        TextView qty = dialog.findViewById(R.id.qty);
                        TextView total = dialog.findViewById(R.id.total);
                        order comanda = new order();
                        comanda.nume = text.getText().toString();
                        comanda.qty = qty.getText().toString();
                        comanda.pret = total.getText().toString();

                        MainActivity.comanda.add(comanda);

                        dialog.dismiss();
                    }
                });



                dialog.show();

            }

        });
    }

    private void setupSearchView() {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.setQueryHint("cauta produs");
    }

    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            mListView.clearTextFilter();
        } else {
            mListView.setFilterText(newText.toString());
        }
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }
}
