package ro.angelogroup.moesisangelo;

import android.app.Activity;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static ro.angelogroup.moesisangelo.MainActivity.user_id;

public class CustomListViewAndroidExample extends Activity {

    ListView list;
    TextView tt;
    Button trimite;
    JSONObject comanda;
    JSONArray produse;
    CustomAdapter adapter;
    public  CustomListViewAndroidExample CustomListView = null;
    //public ArrayList<ListModel> CustomListViewValuesArr = new ArrayList<ListModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_list_view_android_example);

        CustomListView = this;
        trimite = (Button)findViewById(R.id.btn_trimite);

        trimite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("comnda",comanda.toString());

               new SendDeviceDetails().execute("http://angelogroup.ro/api/comanda.php", comanda.toString());

            }
        });


        /******** Take some data in Arraylist ( CustomListViewValuesArr ) ***********/
        //setListData();

        Resources res =getResources();
        tt = ( TextView )findViewById(R.id.text);
        list= ( ListView )findViewById( R.id.list );  // List defined in XML ( See Below )

        /**************** Create Custom Adapter *********/
        adapter=new CustomAdapter( CustomListView, MainActivity.comanda,res );
        list.setAdapter( adapter );
        //Date currentTime = Calendar.getInstance().getTime();
        String currentTime = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date());


        float total = 0;
        produse = new JSONArray();
        int size = MainActivity.comanda.size();
        for (int i=0;i<size;i++){
            JSONObject prod = new JSONObject();
            try {
                prod.put("cantitate",MainActivity.comanda.get(i).qty);
                prod.put("id",MainActivity.comanda.get(i).id);
                //prod.put("name",MainActivity.comanda.get(i).nume);
                //prod.put("price",MainActivity.comanda.get(i).pret);
                produse.put(prod);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            total += Float.parseFloat(MainActivity.comanda.get(i).pret) * Float.parseFloat(MainActivity.comanda.get(i).qty);
        }
        try {
            comanda = new JSONObject();
            comanda.put("orderid","ANDROID-"+currentTime);
            comanda.put("useri", user_id);
            comanda.put("comanda",produse);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        tt.setText((total+"").toString());

    }



    /*****************  This function used by adapter ****************/
    public void onItemClick(int mPosition)
    {
        order tempValues = ( order ) MainActivity.comanda.get(mPosition);


        // SHOW ALERT

        Toast.makeText(CustomListView, ""+tempValues.nume + tempValues.pret,
        Toast.LENGTH_LONG)
                    .show();
    }


     class SendDeviceDetails extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String data = "";

            HttpURLConnection httpURLConnection = null;
            try {

                httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type","application/json");

                httpURLConnection.setDoOutput(true);

                DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                wr.writeBytes(comanda.toString());
                wr.flush();
                wr.close();

                InputStream in = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(in);

                int inputStreamData = inputStreamReader.read();
                while (inputStreamData != -1) {
                    char current = (char) inputStreamData;
                    inputStreamData = inputStreamReader.read();
                    data += current;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("TAG", result);

            try {
                JSONObject obj = new JSONObject(result);
                if (obj.get("mesaj").equals("update")){
                    Toast.makeText(CustomListView, "Comanda a fost trimisa!",
                            Toast.LENGTH_LONG)
                            .show();
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}