package ro.angelogroup.moesisangelo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;



/*
public class MainActivity extends AppCompatActivity  implements View.OnClickListener {

    public Button login_btn;
    public EditText username_field, password_field;
    public Button api_btn;
    public static final String TAG = "response";
    public static String user_id = "";

    JSONObject jObject;
    public static String[] produse;
    public String url_login = "http://angelogroup.ro/api/login.php";
    public String url_produse = "http://angelogroup.ro/api/product/read.php";

    public static ArrayList<data> patiserie, prajituri, prajituriKg, eclere, torturi, diverse;
    public static ArrayList<order> comanda;
    RequestQueue queue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username_field = (EditText)findViewById(R.id.username);
        password_field = (EditText)findViewById(R.id.password);

        queue = Volley.newRequestQueue(this);

        login_btn = (Button) findViewById(R.id.btnLogin);
        login_btn.setOnClickListener(this);

        api_btn = (Button) findViewById(R.id.btnApi);
        api_btn.setVisibility(View.INVISIBLE);
        api_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                String user = username_field.getText().toString();
                String pass = password_field.getText().toString();
                if ((user.length()>1) && (pass.length()>1)){
                    makeJsonObjReq(user, pass);
                } else {
                    Toast.makeText(getApplicationContext(),"(x_x)", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnApi:
                Intent myIntent = new Intent(MainActivity.this,
                        TabActivity.class);
                startActivity(myIntent);
                break;
            default:
                break;
        }
    }

    private void makeJsonObjReq(String username, String password) {

        Map<String, String> postParam= new HashMap<>();
        postParam.put("username", username);
        postParam.put("password", password);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url_login, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        user_id = response.optString("id");
                        makeJsonObjReq2();
                        //new JsonTask().execute(url_produse);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) ;

        jsonObjReq.setTag(TAG);
        queue.add(jsonObjReq);
    }

    private void makeJsonObjReq2() {

        StringRequest postRequest = new StringRequest(Request.Method.POST, url_produse,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            jObject = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(getApplicationContext(),"get produse", Toast.LENGTH_LONG).show();

                        try {
                            getProduct(jObject.getJSONArray("produse"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        api_btn.setVisibility(View.VISIBLE);
                        username_field.setVisibility(View.GONE);
                        password_field.setVisibility(View.GONE);
                        login_btn.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) ;
        queue.add(postRequest);
    }

    private void getProduct(JSONArray jArray){
          patiserie = new ArrayList<>();
          prajituri = new ArrayList<>();
          prajituriKg = new ArrayList<>();
          eclere = new ArrayList<>();
          torturi = new ArrayList<>();
          diverse = new ArrayList<>();
          comanda = new ArrayList<>();

        produse = new String[jArray.length()];
        for (int i = 0; i < jArray.length(); i++) {
            try {
                JSONObject oneObject = jArray.getJSONObject(i);

                data tmp = new data();
                tmp.nume = oneObject.getString("nume");
                tmp.id = oneObject.getString("id");
                tmp.um = oneObject.getString("unitate masura");
                tmp.pret = oneObject.getString("pret");
                tmp.categ = oneObject.getString("categorie");
                tmp.categ_id = oneObject.getString("categorie_id");

                switch(oneObject.getString("categorie_id")) {
                    case "10":
                        diverse.add(tmp);
                        break;
                    case "4":
                        patiserie.add(tmp);
                        break;
                    case "5":
                        prajituri.add(tmp);
                        break;
                    case "1":
                        eclere.add(tmp);
                        break;
                    case "8":
                        torturi.add(tmp);
                        break;
                    case "6":
                        prajituriKg.add(tmp);
                        break;
                }

            } catch (JSONException ignored) {

            }
        }
    }

}
*/

public class MainActivity extends AppCompatActivity {

    public Button login_btn;
    public EditText username_field, password_field;
    public Button api_btn;
    public static final String TAG = "response";
    public static String user_id = "";

    JSONObject jObject;
    public String name, password;
    public static String[] produse;
    public String url_login = "http://angelogroup.ro/api/login.php";
    public String url_produse = "http://angelogroup.ro/api/product/read.php";

    public static ArrayList<data> patiserie, prajituri, prajituriKg, eclere, torturi, diverse;
    public static ArrayList<order> comanda;
    RequestQueue queue;


    @BindView(R.id.input_name) EditText _nameText;
    @BindView(R.id.input_password) EditText _passwordText;
    @BindView(R.id.btn_signup) Button _signupButton;
    @BindView(R.id.link_login) TextView _loginLink;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        queue = Volley.newRequestQueue(this);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                //finish();
            }
        });
    }

    public void signup() {
        Log.d(TAG, "Signup");
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != this.getCurrentFocus())
            imm.hideSoftInputFromWindow(this.getCurrentFocus()
                    .getApplicationWindowToken(), 0);

        if (!validate()) {
            onSignupFailed("");
            return;
        }

        _signupButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Autentificare...");
        progressDialog.show();

        name = _nameText.getText().toString();
        password = _passwordText.getText().toString();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onSignupSuccess or onSignupFailed
                        // depending on success
                        //onSignupSuccess();
                        // onSignupFailed();
                        makeJsonObjReq(name, password);
                        progressDialog.dismiss();
                    }
                }, 2000);
    }


    public void onSignupSuccess() {
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        //finish();
    }

    public void onSignupFailed(String mesaj) {
        Toast.makeText(getBaseContext(), mesaj, Toast.LENGTH_LONG).show();

        _signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String password = _passwordText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameText.setError("cel putin 3 caractere");
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("intre 4 si 10 caractere alfanumerice");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    private void makeJsonObjReq(String username, String password) {

        Map<String, String> postParam= new HashMap<>();
        postParam.put("username", username);
        postParam.put("password", password);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url_login, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        user_id = response.optString("id");
                        String mesaj = "error";
                        mesaj = response.optString("mesaj");
                        if (mesaj.equals("OK")){
                            makeJsonObjReq2();
                        } else {
                            onSignupFailed(mesaj);
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) ;

        jsonObjReq.setTag(TAG);
        queue.add(jsonObjReq);
    }

    private void makeJsonObjReq2() {

        StringRequest postRequest = new StringRequest(Request.Method.POST, url_produse,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            jObject = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(getApplicationContext(),"get produse", Toast.LENGTH_LONG).show();

                        try {
                            getProduct(jObject.getJSONArray("produse"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        finish();
                        Intent myIntent = new Intent(MainActivity.this, TabActivity.class);
                        startActivity(myIntent);

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) ;
        queue.add(postRequest);
    }

    private void getProduct(JSONArray jArray){
        patiserie = new ArrayList<>();
        prajituri = new ArrayList<>();
        prajituriKg = new ArrayList<>();
        eclere = new ArrayList<>();
        torturi = new ArrayList<>();
        diverse = new ArrayList<>();
        comanda = new ArrayList<>();

        produse = new String[jArray.length()];
        for (int i = 0; i < jArray.length(); i++) {
            try {
                JSONObject oneObject = jArray.getJSONObject(i);

                data tmp = new data();
                tmp.nume = oneObject.getString("nume");
                tmp.id = oneObject.getString("id");
                tmp.um = oneObject.getString("unitate masura");
                tmp.pret = oneObject.getString("pret");
                tmp.categ = oneObject.getString("categorie");
                tmp.categ_id = oneObject.getString("categorie_id");

                switch(oneObject.getString("categorie_id")) {
                    case "10":
                        diverse.add(tmp);
                        break;
                    case "4":
                        patiserie.add(tmp);
                        break;
                    case "5":
                        prajituri.add(tmp);
                        break;
                    case "1":
                        eclere.add(tmp);
                        break;
                    case "8":
                        torturi.add(tmp);
                        break;
                    case "6":
                        prajituriKg.add(tmp);
                        break;
                }

            } catch (JSONException ignored) {

            }
        }
    }

}